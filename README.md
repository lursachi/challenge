# eyeo website developer challenge

Please implement a page following the guidelines below.

## Limitations

1. Use HTML, CSS, and JavaScript
2. Don't use libraries, components, plugins, etc
3. Don't include more text or imagery than specified

## Browser support requirements

- IE9+

## Screen size requirements

- 320 x 568 (iPhone 5/SE)
- 600 x 960 (Nexus 7)
- 768 x 1024 (iPad)
- 1366 x 768 (desktop)

And everything in-between landscape and portrait.

## Specifications

### Meta data

| Meta | Contents |
| :-- | :-- |
| title | eyeo website developer challenge |

The page contains a navigation menu and two sections.

### Previews

* [Desktop](/preview/desktop.png)
* [Tablet](/preview/tablets.png)
* [Tablet - menu open](/preview/tablets-menu-open.png)
* [Mobile](/preview/mobile.png)
* [Mobile - menu open](/preview/mobile-menu-open.png)

### Fixed navbar

The navigation menu is fixed, always visible as you scroll, regardless of screen sizes.

**Detail 1:** nav bar background color: `#ed1e45`;

**Detail 2:** nav bar height: `100px`;

There are two links in the navigation menu:
1.  to `#section-1`
2. to `#section-2`

On mobile & tablets, instead of displaying the links inline, there is a toggle button.
When tapped, it opens a submenu containing the links mentioned above.

**Detail:** Background color of the submenu is `#303030`;

Check out the [preview section](#previews) to see what it looks like on smaller screens. 

Asset(s):

* [Menu icon SVG](/assets/menu-toggle.svg)
* [Menu icon PNG](/assets/menu-toggle.png)

### Section 1

#### Image

`https://via.placeholder.com/550x350`

#### Heading

`Lorem ipsum dolor sit amet, consectetur adipiscing elit`

Break at the comma when it makes sense.

#### Paragraph

`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.`

#### Button

`Lorem ipsum dolor sit amet`

**Detail:** Button has background color `#43b77a` and a border radius of `3px`.

### Section 2

Place two components made up of the following content side-by-side on large screens and stacked on small screens.

#### Image

`https://via.placeholder.com/160x160`

#### Heading

`Lorem ipsum dolor sit amet, consectetur adipiscing elit`

Break at the comma when it makes sense.

#### Paragraph

`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.`

**Detail:** The section's background color is `#f3f3f3`.

## Submission

1. Host your submission using Gitlab pages
    - Don't mention anything identifying in your repository (to avoid copycats)
2. Send the URL to your submission to eyeo